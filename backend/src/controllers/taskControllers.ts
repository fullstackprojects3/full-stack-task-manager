import { Request, Response } from "express";
import { PrismaClient } from "@prisma/client";
import jwt, { JwtPayload } from "jsonwebtoken";

// Prisma client instance
const prisma = new PrismaClient();

// Controller function to view all tasks
export const viewTasks = async (req: Request, res: Response) => {
  try {
    // Extract user ID from the request using middleware
    const userId = extractUserId(req);

    // Retrieve tasks for the specific user from the database
    const userTasks = await prisma.task.findMany({
      where: {
        createdById: userId,
      },
    });

    // Send the tasks for the specific user as a response
    res.status(200).json(userTasks);
  } catch (error) {
    // Handle any errors and send an error response
    console.error("Error fetching tasks:", error);
    res.status(500).json({ error: "Internal server error" });
  }
};
// Controller function to add a new task
// Controller function to add a new task
// Middleware to extract user ID from JWT token

// Middleware to extract user ID from JWT token
const extractUserId = (req: Request): string => {
  try {
    // Extract token from authorization header
    const token = req.headers.authorization?.split(" ")[1];
    console.log("token-->", token);

    if (!token) {
      throw new Error("No token provided");
    }

    // Decode token and extract user ID
    const decodedToken = jwt.verify(
      token,
      process.env.JWT_SECRET_KEY as string
    ) as JwtPayload;
    const userId = decodedToken.userId;

    if (!userId) {
      throw new Error("User ID not found in token");
    }

    return userId;
  } catch (error) {
    console.error("Error extracting user ID:", error);
    throw new Error("Failed to extract user ID from token");
  }
};

// Controller function to add a new task
export const addTask = async (req: Request, res: Response) => {
  try {
    // Extract task data from the request body
    const { title, description, dueDate, priority } = req.body;

    const validPriorities = ["High", "Medium", "Low"];
    if (!validPriorities.includes(priority)) {
      res.status(400).json({ error: "Invalid priority value" });
      return;
    }

    // Extract user ID from the request using middleware
    const createdBy = extractUserId(req);

    // Create the task in the database
    const newTask = await prisma.task.create({
      data: {
        title,
        description,
        dueDate: new Date(dueDate).toISOString(),
        priority,
        completed: false, // Assume the task is not completed initially
        createdBy: { connect: { id: createdBy } }, // Connect the task to the user
      },
    });

    // Send the newly created task as a response
    res.status(201).json(newTask);
  } catch (error) {
    // Handle any errors and send an error response
    console.error("Error creating task:", error);
    res.status(500).json({ error: "Internal server error" });
  }
};

// Controller function to update a task

export const updateTask = async (req: Request, res: Response) => {
  try {
    const taskId = req.params.id; // Extract task ID from request parameters as string
    const { title, description, dueDate, priority, completed } = req.body; // Extract updated task data from request body

    // Update the task in the database
    const updatedTask = await prisma.task.update({
      where: { id: taskId }, // Specify which task to update based on its ID
      data: {
        title,
        description,
        dueDate,
        priority,
        completed,
      },
    });

    // Send the updated task as a response
    res.status(200).json(updatedTask);
  } catch (error) {
    // Handle any errors and send an error response
    console.error("Error updating task:", error);
    res.status(500).json({ error: "Internal server error" });
  }
};

// Controller function to delete a task
export const deleteTask = async (req: Request, res: Response) => {
  try {
    const taskId = req.params.id; // Extract task ID from request parameters as string

    // Delete the task from the database
    await prisma.task.delete({
      where: { id: taskId }, // Specify which task to delete based on its ID
    });

    // Send a success response
    res.status(204).end();
  } catch (error) {
    // Handle any errors and send an error response
    console.error("Error deleting task:", error);
    res.status(500).json({ error: "Internal server error" });
  }
};





