import { Request, Response } from "express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { PrismaClient, User } from "@prisma/client";

const prisma = new PrismaClient();

// Use the JWT secret key from environment variables
const JWT_SECRET_KEY: string = process.env.JWT_SECRET_KEY || "";

// Register a new user
export async function register(req: Request, res: Response): Promise<void> {
  const { email, password } = req.body;

  try {
    // Check if the user already exists
    const existingUser: User | null = await prisma.user.findUnique({
      where: { email },
    });

    if (existingUser) {
      res.status(400).json({ error: "User already exists" });
      return;
    }

    // Hash the password
    const hashedPassword = await bcrypt.hash(password, 10);

    // Create a new user in the database
    const newUser: User = await prisma.user.create({
      data: {
        email,
        password: hashedPassword,
      },
    });

    // Respond with the new user's details (without password)
    res.status(201).json({
      id: newUser.id,
      email: newUser.email,
    });
  } catch (error) {
    // Handle the error explicitly
    if (error instanceof Error) {
      res.status(500).json({ error: error.message });
    } else {
      res.status(500).json({ error: "An unknown error occurred" });
    }
  }
}

// Login a user
export async function login(req: Request, res: Response): Promise<void> {
  console.log(req.body);
  const { email, password } = req.body;

  try {
    // Find the user in the database
    const user: User | null = await prisma.user.findUnique({
      where: { email },
    });

    if (!user) {
      res.status(400).json({ error: "User not found" });
      return;
    }

    // Compare the provided password with the stored hashed password
    const isPasswordValid = await bcrypt.compare(password, user.password);

    if (!isPasswordValid) {
      res.status(400).json({ error: "Invalid password" });
      return;
    }

    // Generate a JWT token for the user
    const token = jwt.sign({ userId: user.id }, JWT_SECRET_KEY, {
      expiresIn: "1h",
    });

    // Respond with the token
    res.status(200).json({ token });
  } catch (error) {
    // Handle the error explicitly
    if (error instanceof Error) {
      res.status(500).json({ error: error.message });
    } else {
      res.status(500).json({ error: "An unknown error occurred" });
    }
  }
}
