import express from "express";
import dotenv from "dotenv";
import { PrismaClient } from "@prisma/client";
import authRoutes from "./routes/authRoutes";
import cors from "cors";
import taskRoutes from "./routes/taskRoutes";


// Load environment variables from .env file
dotenv.config();

// Create an Express application
const app = express();

// Enable CORS with default options
const corsOptions = {
    origin:'*', // List the allowed origins here
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    credentials: true,
};

app.use(cors(corsOptions));

// Middleware for parsing JSON requests
app.use(express.json());

// Prisma client instance
const prisma = new PrismaClient();

// Connect to your routes
app.use("/auth", authRoutes);
app.use("/tasks",taskRoutes)

// Handle graceful shutdown by disconnecting the Prisma client
process.on("beforeExit", async () => {
  await prisma.$disconnect();
});

// Listen on a specific port
const PORT = process.env.PORT;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
