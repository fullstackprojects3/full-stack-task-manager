import express from "express";
import { viewTasks, addTask, updateTask, deleteTask } from "../controllers/taskControllers";

const router = express.Router();

// Task routes
router.get('/', viewTasks); // Route for viewing tasks
router.post('/', addTask); // Route for adding a new task
router.put('/:id', updateTask); // Route for updating a task
router.delete('/:id', deleteTask); // Route for deleting a task

export default router;
