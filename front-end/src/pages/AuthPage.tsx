import Login from "../components/auth/Login";
import Register from "../components/auth/Register";
import { Route, Routes } from "react-router-dom";
import { Navigate } from "react-router-dom";

const AuthPage = () => {
  return (
    <Routes>
      {/* Define route for login */}
      <Route path="login" element={<Login />} />

      {/* Define route for register */}
      <Route path="register" element={<Register />} />

      {/* Define catch-all route to redirect to login */}
      <Route path="*" element={<Navigate to="login" />} />
    </Routes>
  );
};

export default AuthPage;
