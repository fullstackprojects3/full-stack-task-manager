import { Link, useNavigate } from "react-router-dom";
import { useForm, SubmitHandler } from "react-hook-form";
import axios, { AxiosError } from "axios";
import { Navigate } from "react-router-dom";
import { useState } from "react";

type FormData = {
  email: string;
  password: string;
};

const Login = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormData>();
  const navigate = useNavigate();

  // State to handle invalid password error
  const [invalidPasswordError, setInvalidPasswordError] = useState(false);

  // Check if a token exists in local storage
  const token = localStorage.getItem("token");

  // If a token exists, redirect the user to the home page
  if (token) {
    return <Navigate to="/home" />;
  }

  const onSubmit: SubmitHandler<FormData> = async (data) => {
    const { email, password } = data;

    try {
      const response = await axios.post("http://localhost:3000/auth/login", {
        email,
        password,
      });

      const { token } = response.data;
      localStorage.setItem("token", token);
      navigate("/home");
    } catch (error) {
      console.error("Login error:", error);

      const axiosError = error as AxiosError;

      // Reset invalid password error state
      setInvalidPasswordError(false);

      if (axiosError.response) {
        // If the response status is 401 (unauthorized), assume invalid password
        if (axiosError.response.status === 400) {
          setInvalidPasswordError(true);
        }
        console.error(
          `HTTP error: ${axiosError.response.status}`,
          axiosError.response.data
        );
      } else if (axiosError.request) {
        console.error(
          "Network error. Please check your internet connection.",
          axiosError.request
        );
      } else {
        console.error(
          "Unexpected error occurred. Please try again.",
          axiosError
        );
      }
    }
  };

  return (
    <div className="flex items-center justify-center h-screen bg-gray-100">
      <div className="w-full max-w-md bg-white shadow-md rounded-lg p-8">
        <h2 className="text-3xl font-semibold mb-6 text-gray-800">Login</h2>

        <form onSubmit={handleSubmit(onSubmit)}>
          {/* Email input */}
          <div className="mb-4">
            <label htmlFor="email" className="block text-gray-700 mb-2">
              Email
            </label>
            <input
              type="email"
              id="email"
              {...register("email", { required: true })}
              className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500"
            />
            {errors.email && (
              <p className="text-red-500 text-sm">Email is required</p>
            )}
          </div>

          {/* Password input */}
          <div className="mb-4">
            <label htmlFor="password" className="block text-gray-700 mb-2">
              Password
            </label>
            <input
              type="password"
              id="password"
              {...register("password", { required: true })}
              className="w-full px-3 py-2 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500"
            />
            {errors.password && (
              <p className="text-red-500 text-sm">Password is required</p>
            )}
            {invalidPasswordError && (
              <p className="text-red-500 text-sm">
                Invalid password. Please try again.
              </p>
            )}
          </div>

          {/* Submit button */}
          <button
            type="submit"
            className="w-full bg-blue-500 hover:bg-blue-600 text-white py-2 rounded-md transition duration-150"
          >
            Login
          </button>
        </form>

        {/* Sign up link */}
        <div className="mt-4 text-center">
          <p className="text-gray-600">
            Don't have an account?{" "}
            <Link to="/auth/register" className="text-blue-500 hover:underline">
              Sign up
            </Link>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Login;
