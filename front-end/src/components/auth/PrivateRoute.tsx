import { ReactElement } from "react";
import { Navigate } from "react-router-dom";

const PrivateRoute = ({ element }: { element: ReactElement }) => {
  const token = localStorage.getItem("token");

  // If there is no token, redirect to login page
  if (!token) {
    return <Navigate to="/auth/login" />;
  }

  // Otherwise, return the passed element
  return element;
};

export default PrivateRoute;
