import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { FaHome, FaEdit, FaTrash, FaSave, FaTimes } from "react-icons/fa";
import axios from "axios";

// Define the structure of a task
interface Task {
  id: string;
  title: string;
  description: string;
  dueDate: string;
  priority: string;
}

// Define the structure of the props in TaskCard
interface TaskCardProps {
  task: Task; // The task object
  className?: string; // Optional className prop for additional styles
  onTaskUpdate: (updatedTask: Task) => void; // Callback function to handle task update
  onTaskDelete: (deletedTaskId: string) => void; // Callback function to handle task deletion
}

interface PriorityColors {
  [key: string]: string;
}

const TaskCard: React.FC<TaskCardProps> = ({
  task,
  className,
  onTaskUpdate,
  onTaskDelete,
}) => {
  const priorityColors: PriorityColors = {
    High: "bg-red-200 text-red-800",
    Medium: "bg-yellow-200 text-yellow-800",
    Low: "bg-green-200 text-green-800",
  };

  const [isEditing, setIsEditing] = useState(false);
  const [editedTitle, setEditedTitle] = useState(task.title);
  const [editedDescription, setEditedDescription] = useState(task.description);

  const handleEditClick = () => {
    setIsEditing(true);
  };

  const handleSaveClick = async () => {
    try {
      // Send API request to update the task
      const response = await axios.put(`http://localhost:3000/tasks/${task.id}`, {
        title: editedTitle,
        description: editedDescription,
        dueDate: task.dueDate,
        priority: task.priority,
        completed: false, // Assuming we're not changing the completed status here
      });

      // Call the callback function to update the task in parent component
      onTaskUpdate(response.data);

      // After successful update, exit edit mode
      setIsEditing(false);
    } catch (error) {
      console.error("Error updating task:", error);
      // Handle error state or display an error message
    }
  };

  const handleCancelClick = () => {
    setEditedTitle(task.title);
    setEditedDescription(task.description);
    setIsEditing(false);
  };

  const handleDeleteClick = async () => {
    try {
      // Send API request to delete the task
      await axios.delete(`http://localhost:3000/tasks/${task.id}`);

      // Remove the deleted task from the UI
      onTaskDelete(task.id);
    } catch (error) {
      console.error("Error deleting task:", error);
      // Handle error state or display an error message
    }
  };

  return (
    <div
      className={`flex flex-col p-6 bg-white rounded-lg shadow-md hover:shadow-lg transition duration-200 ${className}`}
      style={{ maxWidth: "400px" }} // Set max-width for the card
    >
      <div className="flex justify-between items-center mb-4">
        {isEditing ? (
          <>
            <input
              type="text"
              value={editedTitle}
              onChange={(e) => setEditedTitle(e.target.value)}
              className="text-xl font-bold text-gray-800 focus:outline-none"
            />
            <div className="flex space-x-2">
              <button
                onClick={handleSaveClick}
                className="text-green-600 hover:text-green-800 transition duration-200"
              >
                <FaSave className="text-xl" />
              </button>
              <button
                onClick={handleCancelClick}
                className="text-red-600 hover:text-red-800 transition duration-200"
              >
                <FaTimes className="text-xl" />
              </button>
            </div>
          </>
        ) : (
          <>
            <h2 className="text-xl font-bold text-gray-800">{task.title}</h2>
            <div className="flex space-x-2"> {/* Wrap the buttons in a container */}
              <button
                onClick={handleEditClick}
                className="text-green-600 hover:text-green-800 transition duration-200"
              >
                <FaEdit className="text-xl" />
              </button>
              <button
                className="text-red-600 hover:text-red-800 transition duration-200"
                onClick={handleDeleteClick}
              >
                <FaTrash className="text-xl" />
              </button>
            </div>
          </>
        )}
      </div>

      <div className="text-left text-gray-700">
        {isEditing ? (
          <textarea
            value={editedDescription}
            onChange={(e) => setEditedDescription(e.target.value)}
            className="mb-2 w-full border border-gray-300 rounded-md p-2 focus:outline-none"
            rows={3}
          />
        ) : (
          <p className="mb-2">{task.description}</p>
        )}

        <div className="flex space-x-4">
          <div className="bg-gray-200 text-gray-800 rounded-full px-3 py-1 text-sm font-semibold">
            Due: {new Date(task.dueDate).toLocaleDateString()}
          </div>
          <div
            className={`rounded-full px-3 py-1 text-sm font-semibold ${
              priorityColors[task.priority]
            }`}
          >
            Priority: {task.priority}
          </div>
        </div>
      </div>
    </div>
  );
};

const ViewTask: React.FC = () => {
  const [tasks, setTasks] = useState<Task[]>([]); // Initialize with an empty array
  const navigate = useNavigate(); // Initialize the useNavigate hook

  useEffect(() => {
    fetchTasks();
  }, []);

  const fetchTasks = async () => {
    try {
      const token = localStorage.getItem("token");
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      const response = await axios.get("http://localhost:3000/tasks", config);
      setTasks(response.data);
    } catch (error) {
      console.error("Error fetching tasks:", error);
      // Handle error state or display an error message
    }
  };

  const handleTaskUpdate = (updatedTask: Task) => {
    setTasks((prevTasks) =>
      prevTasks.map((task) => (task.id === updatedTask.id ? updatedTask : task))
    );
  };

  const handleTaskDelete = (deletedTaskId: string) => {
    setTasks((prevTasks) => prevTasks.filter((task) => task.id !== deletedTaskId));
  };

  return (
    <div className="flex flex-col w-full min-h-screen bg-gray-50">
      <div className="w-full bg-blue-600 text-white p-4 shadow-md flex justify-between items-center sticky top-0">
        <h1 className="text-xl font-bold">View Tasks</h1>
        <button
          className="flex items-center space-x-2 p-2 bg-white text-blue-600 rounded-lg hover:bg-gray-200 transition duration-200"
          onClick={() => navigate("/")}
        >
          <FaHome className="text-xl" />
          <span>Back to Home</span>
        </button>
      </div>
      <div className="flex flex-wrap gap-6 p-4 h-full">
        {tasks.map((task) => (
          <TaskCard
            key={task.id}
            task={task}
            className="h-full"
            onTaskUpdate={handleTaskUpdate}
            onTaskDelete={handleTaskDelete}
          />
        ))}
      </div>
    </div>
  );
};

export default ViewTask;
