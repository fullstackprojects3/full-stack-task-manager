import { Link, useNavigate } from "react-router-dom";
import { FaTasks, FaPlus, FaSignOutAlt } from "react-icons/fa";
import { useState, useEffect } from "react";
import axios from "axios";

interface Task {
  _id: string;
  title: string;
  description: string;
  dueDate: string;
  priority: string;
  completed: boolean;
  createdAt: string;
  createdById: string;
}


const Home = () => {
  const navigate = useNavigate();
  const [taskSummary, setTaskSummary] = useState({
    pending: 0,
    completed: 0,
    overdue: 0,
  });

  // Function to handle logout
  const handleLogout = () => {
    // Clear local storage
    localStorage.clear();

    // Navigate the user to the login page
    navigate("/auth/login");
  };

  useEffect(() => {
    async function fetchTaskSummary() {
      try {
        const token = localStorage.getItem("token");
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        };
        const response = await axios.get("http://localhost:3000/tasks", config);
        const tasks = response.data;

        const currentDate = new Date();

        let pendingCount = 0;
        let completedCount = 0;
        let overdueCount = 0;

        tasks.forEach((task:Task) => {
          if (!task.completed) {
            pendingCount++;
            if (new Date(task.dueDate) < currentDate) {
              overdueCount++;
            }
          } else {
            completedCount++;
          }
        });

        setTaskSummary({
          pending: pendingCount,
          completed: completedCount,
          overdue: overdueCount,
        });
      } catch (error) {
        console.error("Error fetching task summary:", error);
      }
    }

    fetchTaskSummary();
  }, []);

  return (
    <div className="min-h-screen flex flex-col items-center justify-center text-center bg-gray-50">
      {/* Navigation bar */}
      <div className="w-full flex justify-between items-center fixed top-0 z-10 bg-white p-4 shadow-lg">
        <h1 className="text-2xl font-bold text-gray-800">Task Manager</h1>
        {/* Logout button */}
        <button
          onClick={handleLogout}
          className="flex items-center bg-red-500 hover:bg-red-600 text-white py-2 px-4 rounded-lg transition duration-200"
        >
          <FaSignOutAlt className="mr-2" /> Logout
        </button>
      </div>

      {/* Main container */}
      <div className="w-full md:w-4/5 lg:w-3/5 p-12 bg-white rounded-lg shadow-lg mt-6">
        {/* Main heading */}
        <h1 className="text-4xl font-bold mb-8 text-blue-600">Welcome Back!</h1>

        {/* Task summary */}
        <div className="mb-10">
          <h2 className="text-2xl font-semibold text-gray-700 mb-4">
            Your Task Summary
          </h2>
          <ul className="text-left text-gray-600 space-y-2">
            <li>• {taskSummary.pending} pending tasks</li>
            <li>• {taskSummary.completed} completed tasks</li>
            <li>• {taskSummary.overdue} overdue tasks</li>
          </ul>
        </div>

        {/* Action buttons */}
        <div className="flex justify-center space-x-10">
          {/* View Tasks button */}
          <Link
            to="/tasks"
            className="flex items-center bg-purple-500 hover:bg-purple-600 text-white py-3 px-8 rounded-lg transition duration-200 shadow-md"
          >
            <FaTasks className="mr-2" /> View Tasks
          </Link>

          {/* Add New Task button */}
          <Link
            to="/tasks/new"
            className="flex items-center bg-orange-500 hover:bg-orange-600 text-white py-3 px-8 rounded-lg transition duration-200 shadow-md"
          >
            <FaPlus className="mr-2" /> Add New Task
          </Link>
        </div>

        {/* Render nested routes here */}
      </div>
    </div>
  );
};

export default Home;
