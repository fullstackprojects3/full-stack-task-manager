import { useNavigate } from "react-router-dom";
import { FaCalendarAlt, FaTag, FaPlus, FaTimes, FaHome } from "react-icons/fa";
import { ChangeEvent, FormEvent, useState } from "react";
import axios from "axios";
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const AddTask = () => {
  const navigate = useNavigate();
  const [formData, setFormData] = useState({
    title: "",
    description: "",
    dueDate: "",
    priority: "High",
  });

  const handleInputChange = (
    e: ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    const { name, value } = e.target;
    setFormData((prev) => ({
      ...prev,
      [name]: value,
    }));
  };

  const handleFormSubmit = async (e: FormEvent) => {
    e.preventDefault();
    try {
      const token = localStorage.getItem("token");
      const config = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      const response = await axios.post(
        "http://localhost:3000/tasks",
        { ...formData },
        config
      );
      toast.success("Task added successfully!", {
        position: "top-center",
        autoClose: 2000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      console.log("New task created:", response.data);
      // Clear form data after successful submission
      clearFormData();
    } catch (error) {
      console.error("Error creating task:", error);
      toast.error("Failed to add task. Please try again.", {
        position: "top-center",
        autoClose: 2000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    }
  };

  const clearFormData = () => {
    setFormData({
      title: "",
      description: "",
      dueDate: "",
      priority: "High",
    });
  };

  return (
    <div className="flex flex-col w-full min-h-screen bg-gray-50">
      <div className="w-full bg-blue-600 text-white p-4 shadow-md flex justify-between items-center sticky top-0">
        <h1 className="text-xl font-bold">Add New Task</h1>
        <button
          className="flex items-center space-x-2 p-2 bg-white text-blue-600 rounded-lg hover:bg-gray-200 transition duration-200"
          onClick={() => navigate("/")}
        >
          <FaHome className="text-xl" />
          <span>Back to Home</span>
        </button>
      </div>
      <div className="flex flex-col items-center justify-center w-full md:w-3/5 lg:w-1/2 p-10 bg-white shadow-md rounded-lg mx-auto mt-8">
        <form className="space-y-6" onSubmit={handleFormSubmit}>
          {/* Title input field */}
          <div className="text-left">
            <label
              htmlFor="title"
              className="block text-gray-600 font-medium mb-2"
            >
              Task Title
            </label>
            <input
              id="title"
              name="title"
              type="text"
              placeholder="Enter the title of your task..."
              className="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-purple-500 transition duration-200"
              onChange={handleInputChange}
              value={formData.title}
            />
          </div>
          {/* Description input field */}
          <div className="text-left">
            <label
              htmlFor="task"
              className="block text-gray-600 font-medium mb-2"
            >
              Task Description
            </label>
            <input
              id="task"
              name="description"
              type="text"
              placeholder="Enter your task description..."
              className="w-full px-4 py-3 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-purple-500 transition duration-200"
              onChange={handleInputChange}
              value={formData.description}
            />
          </div>
          <div className="flex flex-col md:flex-row items-start md:items-center space-y-4 md:space-y-0 md:space-x-6">
            <div className="flex items-center space-x-2 w-full md:w-auto">
              <FaCalendarAlt className="text-purple-500" />
              <input
                name="dueDate"
                type="date"
                className="w-full md:w-auto border border-gray-300 rounded-md px-3 py-2 focus:outline-none focus:ring-2 focus:ring-purple-500 transition duration-200"
                onChange={handleInputChange}
                value={formData.dueDate}
              />
            </div>
            <div className="flex items-center space-x-2 w-full md:w-auto">
              <FaTag className="text-purple-500" />
              <select
                name="priority"
                className="w-full md:w-auto border border-gray-300 rounded-md px-3 py-2 focus:outline-none focus:ring-2 focus:ring-purple-500 transition duration-200"
                onChange={handleInputChange}
                value={formData.priority}
              >
                <option value="Low">Low Priority</option>
                <option value="Medium">Medium Priority</option>
                <option value="High">High Priority</option>
              </select>
            </div>
          </div>
          <div className="flex justify-center space-x-6 mt-8">
            <button
              type="submit"
              className="flex items-center bg-purple-600 hover:bg-purple-700 text-white font-semibold py-3 px-6 rounded-lg transition duration-200 shadow-sm"
            >
              <FaPlus className="mr-2" /> Add Task
            </button>
            <button
              type="button"
              onClick={clearFormData} // Call clearFormData function onClick
              className="flex items-center bg-gray-300 hover:bg-gray-400 text-gray-700 font-semibold py-3 px-6 rounded-lg transition duration-200 shadow-sm"
            >
              <FaTimes className="mr-2" /> Clear
            </button>
          </div>
        </form>
      </div>
      <ToastContainer />
    </div>
  );
};

export default AddTask;
