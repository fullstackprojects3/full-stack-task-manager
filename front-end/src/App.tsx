import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import AuthPage from "./pages/AuthPage";
import Home from "./components/home/Home";
import AddTask from "./components/home/AddTask";
import ViewTask from "./components/home/ViewTask";
import PrivateRoute from "./components/auth/PrivateRoute";
import 'react-toastify/dist/ReactToastify.css';

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        {/* Define route for root path */}
        <Route path="/" element={<Navigate to="/auth/login" />} />

        {/* Define route for auth path */}
        <Route path="/auth/*" element={<AuthPage />} />

        {/* Define route for home path */}
        <Route path="/home" element={<PrivateRoute element={<Home />} />} />

        {/* Define route for Add Task */}
        <Route
          path="/tasks/new"
          element={<PrivateRoute element={<AddTask />} />}
        />

        {/* Define route for View Task */}
        <Route
          path="/tasks"
          element={<PrivateRoute element={<ViewTask />} />}
        />

        {/* Define catch-all route to redirect to /auth/login */}
        <Route path="*" element={<Navigate to="/auth/login" />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
